function converter(F) {
    const C = (F - 32) * (5 / 9);
    return C;
}

const tempFs = prompt("Qual a temperatura que você deseja converter de Fahrenheit para Celsius?");
const tempF = parseFloat(tempFs);

if (isNaN(tempF)) {
    console.error("Por favor, insira um valor numérico válido para a temperatura.");
} else {
    const tempC = converter(tempF);
    console.log(tempF + " graus Fahrenheit é equivalente a " + tempC.toFixed(2) + " graus Celsius");
}