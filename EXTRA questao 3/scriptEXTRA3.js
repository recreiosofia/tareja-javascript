let valores = [4, 1, 7, 2, 9];

valores.sort(function(a, b) {
    return a - b;
});

console.log("Valores em ordem crescente:", valores);

console.log();

console.log("Valores na sequência:", valores.join(", "));