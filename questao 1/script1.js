function multiplicacao (Mat1, Mat2) {
    const linhaMat1 = Mat1.length;
    const colunaMat1 = Mat1[0].length;
    const linhaMat2 = Mat2.length;
    const colunaMat2 = Mat2[0].length;

    if (colunaMat1 != linhaMat2) {
        console.error("Oops! O número de colunas da primeira mtariz deve ser igual ao número de linhas da segunda matriz");
        return;
    }

    const resultado = new Array(linhaMat1);

    for (let i = 0; i < linhaMat1; i++) {
        resultado[i] = new Array(colunaMat2).fill(0);
    }

    for (let i = 0; i < linhaMat1; i++) {
        for (let j = 0; j < colunaMat2; j++) {
            for (let k = 0; k < colunaMat1; k++) {
                resultado[i][j] += Mat1[i][k] * Mat2[k][j];
            }
        }
    }

    return resultado;

}

//Testando caso A

const Mat1Teste1 = [ [ [2],[-1] ], [ [2],[0] ] ];
const Mat2Teste1 = [ [2,3],[-2,1] ];

console.log("Resultado caso A: ", multiplicacao(Mat1Teste1, Mat2Teste1));

//Testando caso B

const Mat1Teste2 = [ [4,0], [-1,-1] ];
const Mat2Teste2 = [ [-1,3], [2,7] ];

console.log("Resultado caso B: ", multiplicacao(Mat1Teste2, Mat2Teste2));