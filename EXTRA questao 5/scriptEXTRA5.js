const booksByCategory = [
    {
        category: "Riqueza",
        books: [
            {
                title: "Os segredos da mente milionária",
                author: "T. Harv Eker",
            },
            {
                title: "O homem mais rico da Babilônia",
                author: "George S. Clason",
            },
            {
                title: "Pai rico, pai pobre",
                author: "Robert T. Kiyosaki e Sharon L. Lechter",
            },
        ],
    },
    {
        category: "Inteligência Emocional",
        books: [
            {
                title: "Você é Insubstituível",
                author: "Augusto Cury",
            },
            {
                title: "Ansiedade – Como enfrentar o mal do século",
                author: "Augusto Cury",
            },
            {
                title: "Os 7 hábitos das pessoas altamente eficazes",
                author: "Stephen R. Covey"
            }
        ]
    }
];

// Desafio 1

function contarCategorias() {
    let categories = booksperCat.length;
    console.log("Total de Categorias: " + categories);

    for (let category of booksperCat) {
        let totalbooks = category.books.length;
        console.log("Categoria: " + category.category + " - Total de Livros: " + totalbooks);
    }
}

// Desafio 2

function contarAutores() {
    let authors = new Set();

    for (let category of booksperCat) {
        for (let book of category.books) {
            authors.add(book.author);
        }
    }

    console.log("Total de autores: " + authors.size);
}

// Desafio 3

function mostrarLivrosCurry() {
    let booksCury = [];

    for (let category of booksperCat) {
        for (let book of category.books) {
            if (book.author === "Augusto Curry") {
                booksCury.push(book.title);
            }
        }
    }

    
    if (booksCury === 0) {
        console.log("Não há nenhum livro do autor Augusto Curry");
    } else {
        console.log("Livros do autor Augusto Cury: " + booksCury.join(", "));
    }
}

// Desafio 4

function mostrarLivros (name) {
    let booksofauthor = [];

    for (let category of booksperCat) {
        for (let book of category.books) {
            if (book.author === name) {
                booksofauthor.push(book.title);
            }
        }
    }

    if (booksofauthor === 0) {
        console.log("Não há nenhum livro do autor " + name);
    } else {
        console.log("Livros do autor: " + name + booksofauthor.join(", "));
    }
}

contarCategorias();
contarAutores();
mostrarLivrosCurry();
mostrarLivros ();