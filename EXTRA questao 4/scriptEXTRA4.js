function converter(nota) {
    if (nota >= 90) {
        return "A";
    } else {
        if (nota >= 80) {
            return "B";
        } else {
            if (nota >= 70) {
                return "C";
            } else {
                if (nota >= 60) {
                    return "D";
                } else {
                    return "F";
                }
            }
        }
    }
}

const num = parseInt(prompt("Quantas notas serão?"));
let notas = [];

for (let i = 0; i < num; i++) {
    let valor = parseFloat(prompt("Qual a nota " + i + "?"));
    notas.push(valor);
    console.log("Nota " + notas[i] + ": " + converter(notas[i]));
}


