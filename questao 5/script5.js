for (let i = 0; i < 5; i++) {
    const numero = Math.floor(Math.random() * 100) + 1;

    if (numero % 3 === 0 && numero % 5 === 0) {
        console.log(numero + " fizzbuzz");
    } else if (numero % 3 === 0) {
        console.log(numero + " fizz");
    } else if (numero % 5 === 0) {
        console.log(numero + "buzz");
    } else {
        console.log(numero);
    }
}