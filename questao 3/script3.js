function calcularMedia(notas) {
    const soma = notas.reduce((soma, nota) => soma + nota, 0);
    const media = soma / 3;
    return media;
  }
  
  const nota1 = parseFloat(prompt("Digite a primeira nota: "));
  const nota2 = parseFloat(prompt("Digite a segunda nota: "));
  const nota3 = parseFloat(prompt("Digite a terceira nota: "));
  
  if (isNaN(nota1) || isNaN(nota2) || isNaN(nota3)) {
    console.error("Por favor, insira valores numéricos válidos para as notas.");
  } else {
    const notas = [nota1, nota2, nota3];
    const media = calcularMedia(notas);
  
    if (media >= 6) {
      console.log("Aprovado! Média: " + media.toFixed(2));
    } else {
      console.log("Reprovado. Média: " + media.toFixed(2));
    }
  }