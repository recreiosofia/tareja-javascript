const familia = {
    receitas: [
        {
            falseilha: 1000, 
            packageai: 1500, 
            mãe : 2000,
        }
    ],
    despesas: [
        {
            água: 600, 
            luz: 800, 
            comida:1000,
        }
    ]
};
  
function calculo() {
    const totalReceitas = familia.receitas.reduce((acc, value) => acc + value, 0);
    const totalDespesas = familia.despesas.reduce((acc, value) => acc + value, 0);
  
    const saldo = totalReceitas - totalDespesas;
  
    // Verificando se o saldo é positivo ou negativo
    if (saldo >= 0) {
      console.log(`A família está com saldo positivo de R$${saldo.toFixed(2)}.`);
    } else {
      console.log(`A família está com saldo negativo de R$${Math.abs(saldo).toFixed(2)}.`);
    }
}
  
calculo();